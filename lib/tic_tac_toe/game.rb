module TicTacToe
  class Game
    attr_reader :players , :board , :current_player , :other_player
    def initialize(players ,board = Board.new)
      @players = players
      @board= board
      @current_player , @other_player = players.shuffle
    end

    def switch_players
      @current_player , @other_player = @other_player , @current_player
    end

    def solicit_move
      "#{current_player.name}: Enter a number between 1 and 9 to make your move"
    end
    
    def get_move(human_move = gets.chomp)
      human_move_to_coordinate(human_move)
    end

    def game_over_message
      return "#{current_player.name} won!" if board.end_game == :winner
      return "The game ended in a tie" if board.end_game == :draw
    end

    def play
      puts "1 2 3"
      puts "4 5 6"
      puts "7 8 9"
      puts "#{current_player.name} as randomly been selected to play first"
      while true
        board.show_grid
	puts ""
	puts solicit_move
	x , y = get_move
	board.set( x , y , current_player.color)
	if board.end_game
	  puts game_over_message
	  board.show_grid
	  return
	else
	  switch_players
	end
      end
    end

    private
      def human_move_to_coordinate (move)
        mapping={
	  "1" => [ 0 , 0 ],          
	  "2" => [ 0 , 1 ],
	  "3" => [ 0 , 2 ],
	  "4" => [ 1 , 0 ],
	  "5" => [ 1 , 1 ],
	  "6" => [ 1 , 2 ],
	  "7" => [ 2 , 0 ],
	  "8" => [ 2 , 1 ],
	  "9" => [ 2 , 2 ]
	}
	mapping[move]
      end
  end
end
