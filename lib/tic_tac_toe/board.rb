module TicTacToe
  class Board
    attr_reader :grid
    def initialize(input={})
      @grid= input.fetch(:grid , default_board)
    end

    def get (x , y)
      grid[x][y]
    end

    def  set (x , y , value)
      get(x,y).value = value
    end

    def draw?
      grid.flatten.map{ |cell| cell.value } .none_empty?
    end

    def diagonals
      [
        [get(0,0) , get(1,1) , get(2,2)],
	[get(0,2) , get(1,1) , get(2,0)]
      ]
    end

    def winning_pos
      grid + 
      grid.transpose +
      diagonals
    end

    def winning_pos_value(winning_pos)
      winning_pos.map{ |cell| cell.value}
    end

    def winner?
      winning_pos.each do |winning_pos|
        next if winning_pos_value(winning_pos).all_empty?
	return true if winning_pos_value(winning_pos).all_same?
      end
	false
    end
   
    def end_game
      return :winner if winner?
      return :draw if draw?
      false
    end

    def show_grid
      grid.each do |row|
        puts row.map { |cell| cell.value.empty? ? "_" : cell.value }.join(" ")
      end
    end
    
    private
      def default_board
        Array.new(3) { Array.new(3) { Cell.new } }
      end
  end
end
