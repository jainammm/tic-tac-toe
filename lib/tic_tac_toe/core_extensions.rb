class Array
  def all_empty?
    self.all? {|s| s.to_s.empty? }
  end

  def all_same?
    self.all? {|s| s == self[0] }
  end

  def any_empty? 
    self.any? {|s| s.to_s.empty? }
  end

  def none_empty?
    !any_empty?
  end
end
