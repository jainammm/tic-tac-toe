require "spec_helper.rb"

module TicTacToe
  describe Player do
    
    context "#name" do
      it "returns name of player" do 
        hash = { color: "X" , name: "Jainam"}
	player = Player.new(hash)
	expect(player.name) .to eq("Jainam")
      end
    end

    context "#color" do
      it "returns color of player" do
        hash = { color: "X" , name: "Jainam"}
        player = Player.new(hash)
        expect(player.color) .to eq("X")
      end
    end
  end
end
