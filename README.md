# Tic-Tac-Toe
It runs a game of tic-tac-toe between two human players. The players select their move by typing 1-9 as in pattern shown.
1 | 2 | 3
4 | 5 | 6
7 | 8 | 9
Also this code Object Oriented! Having classes like cell, board, player and game. All classes have their own responsibility.

## How to create ruby gem
Run ` bundle gem tic_tac_toe ` to create a repository and also create a directory structure.

## Cell Class
It just has state and no behaviour. It stores a state of a cell of board, i.e. the value of that cell, whether empty or "X" or "O".

## Player Class
It has the name and symbol of the the Player.

## Board Class
It contains the board of our game in form of a 2D array. The elements of 2D array are objects of class cell. We can get and set a value of a cell. We can get the winning-pos and also if game has ended with a winner it has drawn. We can also show the grid of our game.

## Game CLass
It has state of player, board, current-player, other-player. Its behaviour is to play a game and take move from player and set it in board.

## Run specs
You can run the test cases by running `rspec`.
