root = File.expand_path("../", File.dirname(__FILE__))
require "#{root}/lib/tic_tac_toe.rb"

puts "Welcome to tic tac toe"
Jainam = TicTacToe::Player.new({color: "X", name: "Jainam"})
The_one_losing = TicTacToe::Player.new({color: "O", name: "The_one_losing"})
players = [Jainam, The_one_losing]
TicTacToe::Game.new(players).play
